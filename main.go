package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"os"

	"github.com/golang/glog"
	"gitlab.com/demo-micro-services/demo-service/handler"
	pb "gitlab.com/demo-micro-services/protobuf/demo-service"
	"google.golang.org/grpc"
)

var (
	portCoreDemo = 50051
)

func main() {
	os.Args = append(os.Args, "-logtostderr=true")
	flag.Parse()
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", portCoreDemo))
	if err != nil {
		glog.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterDemoServiceServer(s, handler.NewDemoServiceHandler())
	glog.Infof("server listening at %v", lis.Addr())
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
