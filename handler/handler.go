package handler

import (
	"context"

	"github.com/golang/glog"

	pb "gitlab.com/demo-micro-services/protobuf/demo-service"
)

type demoServiceHandler struct {
}

func NewDemoServiceHandler() pb.DemoServiceServer {
	return &demoServiceHandler{}
}

func (h *demoServiceHandler) ListOrders(ctx context.Context, req *pb.ListOrdersRequest) (*pb.ListOrdersResponse, error) {
	glog.Infof("[ListOrders] req: %+v", req)
	// to do
	return &pb.ListOrdersResponse{
		Data:         make([]*pb.Order, 0),
		TotalRecords: 0,
	}, nil
}
